# Gitlab Runner Docker

This Docker image provides a Gitlab runner that can be easily configured and deployed within a Docker environment. It allows you to run Gitlab runner pipelines on your own infrastructure.

## Docker hub

Find the Docker image on Docker Hub: https://hub.docker.com/repository/docker/dalil01/gitlab-runner

## Build

To build the Docker image, use the following command:

```
docker build --no-cache -t gitlab-runner:1.0.0 .
```

## Run

```
 docker run -it -d --name gitlab-runner --restart always -e URL=<URL> -e TOKEN=<TOKEN> -e RUNNER_ALLOW_RUNASROOT="1" -v /var/run/docker.sock:/var/run/docker.sock gitlab-runner:1.0.0
```

Replace `<URL>` and `<TOKEN>` with your desired values.

## Resources

Find additional documentation: https://docs.gitlab.com/runner/

## Contribution

Want to improve this project? Feel free to open merge requests and create issues.